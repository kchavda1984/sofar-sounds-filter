# Sofar Sounds filter web app

Develop a single page application which allows the user to filter shows by genre.  

### Technologies used
- Frontend SASS Boilerplate - Primitive.
- jQuery to call JSON data and create filter functionality

All documentation for Primitive can be found at [https://taniarascia.github.io/primitive]

## Getting Started
- clone repo [https://kchavda1984@bitbucket.org/kchavda1984/sofar-sounds-filter.git]
- cd sofar-sounds-filter
- npm install
- gulp