$(function() {
    // declare variables
    var genre_filter = $('#genre-filter li');
    
    // on click select genre tab
    genre_filter.click(function () {
        event.filter_genre($(this).attr("data-genre-type"));
    });
        
    var event = {
        
        // load events from JSON file
        load_events: function() {
            $.ajax({
                type: 'Get',
                url: './data/shows.json',
                dataType: 'json',
                success: function(data) {
                    events.list = data;
                    event.populate_events();
                }
            })
        },

        // populate events list
        populate_events: function() {

            var event_list = $('#events');

            // loop through JSON file
            $.each(events.list, function(i, key) {
                event_list.append('<li class="flex-row event" data-genre="'
                + key.genre +'"><div class="event-date"><span class="date">' 
                + key.date.dayOfMonth + '</span><span class="month">'
                + key.date.month +'</span></div><div class="event-detail"><h3 class="day">' 
                + key.date.dayOfWeek + '</h3><h2 class="loc">'
                + key.location +'</h2></div></li>');
            });
        },

        // filter through genre
        filter_genre: function(genre_type) {

            var genre_all = $('[data-genre-type="all"]');
            var event = $('.event');

            // if statement to show active tab
            if(genre_type == "all") {
                genre_filter.removeClass('filter-active');
                event.show();
                genre_all.addClass('filter-active');
            } else {
                genre_filter.removeClass('filter-active');
                event.hide();
                $('[data-genre="'+ genre_type +'"]').show();
                $('[data-genre-type="' + genre_type + '"]').addClass('filter-active');
            }
        }
    };

    // invoke function
    event.load_events();

});

